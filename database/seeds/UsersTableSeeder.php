<?php

use App\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    public function run()
    {
        $users = [
            [
                'id'             => 1,
                'name'           => 'Administrador',
                'email'          => 'admin@mail.com',
                'password'       => '$2y$10$h5WW.OXyYtHu6k1V4BzoAuSVyE37C6t.7BDS./sDSh7Ji7VsUOu4u',
                'remember_token' => null,
            ],
        ];

        User::insert($users);
    }
}
