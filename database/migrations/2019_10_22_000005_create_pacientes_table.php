<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePacientesTable extends Migration
{
    public function up()
    {
        Schema::create('pacientes', function (Blueprint $table) {
            $table->increments('id');

            $table->string('nome');

            $table->string('dni');
            
            $table->string('codigo')->nullable();

            $table->date('nascimento');

            $table->string('sexo');

            $table->string('area');

            $table->string('sanguineo');

            $table->string('civil');

            $table->string('email')->nullable();

            $table->string('direccion');

            $table->string('urban');

            $table->string('cidade');

            $table->string('estado');

            $table->string('telefono');

            $table->longText('observacoes')->nullable();

            $table->timestamps();

            $table->softDeletes();
        });
    }
}
