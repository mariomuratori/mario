@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.create') }} {{ trans('cruds.atendimento.title_singular') }}
    </div>

    <div class="card-body">
        <form action="{{ route("admin.atendimentos.store") }}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="form-group {{ $errors->has('paciente_id') ? 'has-error' : '' }}">
                <label for="paciente">{{ trans('cruds.atendimento.fields.paciente') }}*</label>
                <select name="paciente_id" id="paciente" class="form-control select2" required>
                    @foreach($pacientes as $id => $paciente)
                        <option value="{{ $id }}" {{ (isset($atendimento) && $atendimento->paciente ? $atendimento->paciente->id : old('paciente_id')) == $id ? 'selected' : '' }}>{{ $paciente }}</option>
                    @endforeach
                </select>
                @if($errors->has('paciente_id'))
                    <p class="help-block">
                        {{ $errors->first('paciente_id') }}
                    </p>
                @endif
            </div>
            <div class="form-group {{ $errors->has('data') ? 'has-error' : '' }}">
                <label for="data">{{ trans('cruds.atendimento.fields.data') }}*</label>
                <input type="text" id="data" name="data" class="form-control date" value="{{ old('data', isset($atendimento) ? $atendimento->data : '') }}" required>
                @if($errors->has('data'))
                    <p class="help-block">
                        {{ $errors->first('data') }}
                    </p>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.atendimento.fields.data_helper') }}
                </p>
            </div>
            <div class="form-group {{ $errors->has('hora') ? 'has-error' : '' }}">
                <label for="hora">{{ trans('cruds.atendimento.fields.hora') }}*</label>
                <input type="text" id="hora" name="hora" class="form-control timepicker" value="{{ old('hora', isset($atendimento) ? $atendimento->hora : '') }}" required>
                @if($errors->has('hora'))
                    <p class="help-block">
                        {{ $errors->first('hora') }}
                    </p>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.atendimento.fields.hora_helper') }}
                </p>
            </div>
            <div class="form-group {{ $errors->has('service_id') ? 'has-error' : '' }}">
                <label for="service">{{ trans('cruds.atendimento.fields.service') }}*</label>
                <select name="service_id" id="service" class="form-control select2" required>
                    @foreach($services as $id => $service)
                        <option value="{{ $id }}" {{ (isset($atendimento) && $atendimento->service ? $atendimento->service->id : old('service_id')) == $id ? 'selected' : '' }}>{{ $service }}</option>
                    @endforeach
                </select>
                @if($errors->has('service_id'))
                    <p class="help-block">
                        {{ $errors->first('service_id') }}
                    </p>
                @endif
            </div>

            <div class="form-group {{ $errors->has('asistencia') ? 'has-error' : '' }}">
                <label>{{ trans('cruds.atendimento.fields.asistencia') }}:</i></label>
                @foreach(App\Atendimento::A_Asistencia as $key => $label)
                    <div>
                        <input id="asistencia_{{ $key }}" name="asistencia" type="radio" value="{{ $key }}" {{ old('asistencia', null) === (string)$key ? 'checked' : '' }} required>
                        <label for="asistencia_{{ $key }}">{{ $label }}</label>
                    </div>
                @endforeach
                @if($errors->has('asistencia'))
                    <p class="help-block">
                        {{ $errors->first('asistencia') }}
                    </p>
                @endif
            </div>


            <div class="form-group {{ $errors->has('peso') ? 'has-error' : '' }}">
                <label for="peso">{{ trans('cruds.atendimento.fields.peso') }}*</label>
                <input type="text" id="peso" name="peso" class="form-control" value="{{ old('peso', isset($atendimento) ? $atendimento->peso : '') }}" required>
                @if($errors->has('peso'))
                    <p class="help-block">
                        {{ $errors->first('peso') }}
                    </p>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.atendimento.fields.peso_helper') }}
                </p>
            </div>
            <div class="form-group {{ $errors->has('talla') ? 'has-error' : '' }}">
                <label for="talla">{{ trans('cruds.atendimento.fields.talla') }}*</label>
                <input type="text" id="talla" name="talla" class="form-control" value="{{ old('talla', isset($atendimento) ? $atendimento->talla : '') }}" required>
                @if($errors->has('talla'))
                    <p class="help-block">
                        {{ $errors->first('talla') }}
                    </p>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.atendimento.fields.talla_helper') }}
                </p>
            </div>
            <div class="form-group {{ $errors->has('presion') ? 'has-error' : '' }}">
                <label for="presion">{{ trans('cruds.atendimento.fields.presion') }}*</label>
                <input type="text" id="presion" name="presion" class="form-control" value="{{ old('presion', isset($atendimento) ? $atendimento->presion : '') }}" required>
                @if($errors->has('presion'))
                    <p class="help-block">
                        {{ $errors->first('presion') }}
                    </p>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.atendimento.fields.presion_helper') }}
                </p>
            </div>
            <div class="form-group {{ $errors->has('fcard') ? 'has-error' : '' }}">
                <label for="fcard">{{ trans('cruds.atendimento.fields.fcard') }}*</label>
                <input type="text" id="fcard" name="fcard" class="form-control" value="{{ old('fcard', isset($atendimento) ? $atendimento->fcard : '') }}" required>
                @if($errors->has('fcard'))
                    <p class="help-block">
                        {{ $errors->first('fcard') }}
                    </p>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.atendimento.fields.fcard_helper') }}
                </p>
            </div>
            <div class="form-group {{ $errors->has('fresp') ? 'has-error' : '' }}">
                <label for="fresp">{{ trans('cruds.atendimento.fields.fresp') }}*</label>
                <input type="text" id="fresp" name="fresp" class="form-control" value="{{ old('fresp', isset($atendimento) ? $atendimento->fresp : '') }}" required>
                @if($errors->has('fresp'))
                    <p class="help-block">
                        {{ $errors->first('fresp') }}
                    </p>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.atendimento.fields.fresp_helper') }}
                </p>
            </div>
            <div class="form-group {{ $errors->has('temperatura') ? 'has-error' : '' }}">
                <label for="temperatura">{{ trans('cruds.atendimento.fields.temperatura') }}*</label>
                <input type="text" id="temperatura" name="temperatura" class="form-control" value="{{ old('temperatura', isset($atendimento) ? $atendimento->temperatura : '') }}" required>
                @if($errors->has('temperatura'))
                    <p class="help-block">
                        {{ $errors->first('temperatura') }}
                    </p>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.atendimento.fields.temperatura_helper') }}
                </p>
            </div>
            <div class="form-group {{ $errors->has('observacoes') ? 'has-error' : '' }}">
                <label for="observacoes">{{ trans('cruds.atendimento.fields.observacoes') }}</label>
                <textarea id="observacoes" name="observacoes" class="form-control ">{{ old('observacoes', isset($atendimento) ? $atendimento->observacoes : '') }}</textarea>
                @if($errors->has('observacoes'))
                    <p class="help-block">
                        {{ $errors->first('observacoes') }}
                    </p>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.atendimento.fields.observacoes_helper') }}
                </p>
            </div>
            <div>
                <input class="btn btn-danger" type="submit" value="{{ trans('global.save') }}">
            </div>
        </form>
    </div>
</div>
@endsection

@section('scripts')
<script>
    var uploadedDocumentoMap = {}
Dropzone.options.documentoDropzone = {
    url: '{{ route('admin.atendimentos.storeMedia') }}',
    maxFilesize: 10, // MB
    addRemoveLinks: true,
    headers: {
      'X-CSRF-TOKEN': "{{ csrf_token() }}"
    },
    params: {
      size: 10
    },
    success: function (file, response) {
      $('form').append('<input type="hidden" name="documento[]" value="' + response.name + '">')
      uploadedDocumentoMap[file.name] = response.name
    },
    removedfile: function (file) {
      file.previewElement.remove()
      var name = ''
      if (typeof file.file_name !== 'undefined') {
        name = file.file_name
      } else {
        name = uploadedDocumentoMap[file.name]
      }
      $('form').find('input[name="documento[]"][value="' + name + '"]').remove()
    },
    init: function () {
@if(isset($atendimento) && $atendimento->documento)
          var files =
            {!! json_encode($atendimento->documento) !!}
              for (var i in files) {
              var file = files[i]
              this.options.addedfile.call(this, file)
              file.previewElement.classList.add('dz-complete')
              $('form').append('<input type="hidden" name="documento[]" value="' + file.file_name + '">')
            }
@endif
    },
     error: function (file, response) {
         if ($.type(response) === 'string') {
             var message = response //dropzone sends it's own error messages in string
         } else {
             var message = response.errors.file
         }
         file.previewElement.classList.add('dz-error')
         _ref = file.previewElement.querySelectorAll('[data-dz-errormessage]')
         _results = []
         for (_i = 0, _len = _ref.length; _i < _len; _i++) {
             node = _ref[_i]
             _results.push(node.textContent = message)
         }

         return _results
     }
}
</script>
@stop