@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.create') }} {{ trans('cruds.paciente.title_singular') }}
    </div>

    <div class="card-body">
        <form action="{{ route("admin.pacientes.store") }}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="form-group {{ $errors->has('nome') ? 'has-error' : '' }}">
                <label for="nome">{{ trans('cruds.paciente.fields.nome') }}*</label>
                <input type="text" id="nome" name="nome" class="form-control" value="{{ old('nome', isset($paciente) ? $paciente->nome : '') }}" required>
                @if($errors->has('nome'))
                    <p class="help-block">
                        {{ $errors->first('nome') }}
                    </p>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.paciente.fields.nome_helper') }}
                </p>
            </div>
            <div class="form-group {{ $errors->has('dni') ? 'has-error' : '' }}">
                <label for="dni">{{ trans('cruds.paciente.fields.dni') }}*</label>
                <input type="text" id="dni" name="dni" class="form-control" value="{{ old('dni', isset($paciente) ? $paciente->dni : '') }}" required>
                @if($errors->has('dni'))
                    <p class="help-block">
                        {{ $errors->first('dni') }}
                    </p>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.paciente.fields.dni_helper') }}
                </p>
            </div>
            <div class="form-group {{ $errors->has('codigo') ? 'has-error' : '' }}">
                <label for="codigo">{{ trans('cruds.paciente.fields.codigo') }}*</label>
                <input type="text" id="codigo" name="codigo" class="form-control" value="{{ old('codigo', isset($paciente) ? $paciente->codigo : '') }}" required>
                @if($errors->has('codigo'))
                    <p class="help-block">
                        {{ $errors->first('codigo') }}
                    </p>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.paciente.fields.codigo_helper') }}
                </p>
            </div>
            <div class="form-group {{ $errors->has('nascimento') ? 'has-error' : '' }}">
                <label for="nascimento">{{ trans('cruds.paciente.fields.nascimento') }}*</label>
                <input type="text" id="nascimento" name="nascimento" class="form-control date" value="{{ old('nascimento', isset($paciente) ? $paciente->nascimento : '') }}" required>
                @if($errors->has('nascimento'))
                    <p class="help-block">
                        {{ $errors->first('nascimento') }}
                    </p>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.paciente.fields.nascimento_helper') }}
                </p>
            </div>
            <div class="form-group {{ $errors->has('sexo') ? 'has-error' : '' }}">
                <label>{{ trans('cruds.paciente.fields.sexo') }}* <i class="fas fa-venus-mars"></i></label>
                @foreach(App\Paciente::SEXO_RADIO as $key => $label)
                    <div>
                        <input id="sexo_{{ $key }}" name="sexo" type="radio" value="{{ $key }}" {{ old('sexo', null) === (string)$key ? 'checked' : '' }} required>
                        <label for="sexo_{{ $key }}">{{ $label }}</label>
                    </div>
                @endforeach
                @if($errors->has('sexo'))
                    <p class="help-block">
                        {{ $errors->first('sexo') }}
                    </p>
                @endif
            </div>

            <div class="form-group {{ $errors->has('civil') ? 'has-error' : '' }}">
           <label for="civil">{{ trans('cruds.paciente.fields.civil') }}</label>
           @foreach(App\Paciente::E_Civil as $key => $label)
           <select id="civil_{{ $key }}" name="civil" id="civil" class="form-control" style="width: 100%;" required>
           <div>
                        <option id="civil_{{ $key }}" name="civil" value="{{ $key }}" {{ old('civil', null) === (string)$key ? 'checked' : '' }} required>
                        <label for="civil_{{ $key }}">{{ $label }}</label>
                    </div>       
           <option >Soltero</option>
                    <option>Casado</option>
                    <option>Viudo</option>
                    <option>Separado</option>
                    <option>Divorciado</option>
                  </select>
                  @endforeach
                  @if($errors->has('civil'))
                    <p class="help-block">
                        {{ $errors->first('civil') }}
                    </p>
                @endif
            </div>
  <p>




            <div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
                <label for="email">{{ trans('cruds.paciente.fields.email') }}</label>
                <input type="email" id="email" name="email" class="form-control" value="{{ old('email', isset($paciente) ? $paciente->email : '') }}">
                @if($errors->has('email'))
                    <p class="help-block">
                        {{ $errors->first('email') }}
                    </p>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.paciente.fields.email_helper') }}
                </p>
            </div>
            <div class="form-group {{ $errors->has('area') ? 'has-error' : '' }}">
                <label>{{ trans('cruds.paciente.fields.area') }}*</label>
                 @foreach(App\Paciente::AREA_SELECT as $key => $label)
                    <div>
                        <input id="area_{{ $key }}" name="area" type="radio" value="{{ $key }}" {{ old('area', null) === (string)$key ? 'checked' : '' }} required>
                        <label for="area_{{ $key }}">{{ $label }}</label>
                    </div>
                @endforeach
                @if($errors->has('area'))
                    <p class="help-block">
                        {{ $errors->first('area') }}
                    </p>
                @endif
            </div>

            





            <div class="form-group">
    <label for="exampleFormControlSelect1">{{ trans('cruds.paciente.fields.alergia') }} <i class="fas fa-allergies"></i></label>
    <select class="form-control" id="exampleFormControlSelect1">
    <option>Ninguna</option>
      <option>Picaduras de insectos</option>
      <option>Medicinas</option>
      <option>Productos químicos</option>
      <option>Mascotas</option>
      <option>Alimentos</option>
    </select>
  </div>
  <p>


  <div class="form-group {{ $errors->has('sanguineo') ? 'has-error' : '' }}">
                <label>{{ trans('cruds.paciente.fields.sanguineo') }}*</label>
                 @foreach(App\Paciente::G_Sanguineo as $key => $label)
                    <div>
                        <input id="sanguineo_{{ $key }}" name="sanguineo" type="radio" value="{{ $key }}" {{ old('sanguineo', null) === (string)$key ? 'checked' : '' }} required>
                        <label for="sanguineo_{{ $key }}">{{ $label }}</label>
                    </div>
                @endforeach
                @if($errors->has('sanguineo'))
                    <p class="help-block">
                        {{ $errors->first('sanguineo') }}
                    </p>
                @endif
            </div>




            <div class="form-group {{ $errors->has('telefono') ? 'has-error' : '' }}">
                <label for="telefono">{{ trans('cruds.paciente.fields.telefono') }}*</label>
                <input type="text" id="telefono" name="telefono" class="form-control" value="{{ old('telefono', isset($paciente) ? $paciente->telefono : '') }}" required>
                @if($errors->has('telefono'))
                    <p class="help-block">
                        {{ $errors->first('telefono') }}
                    </p>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.paciente.fields.telefono_helper') }}
                </p>
            </div>
            <div class="form-group {{ $errors->has('direccion') ? 'has-error' : '' }}">
                <label for="direccion">{{ trans('cruds.paciente.fields.direccion') }}*</label>
                <input type="text" id="direccion" name="direccion" class="form-control" value="{{ old('direccion', isset($paciente) ? $paciente->direccion : '') }}" required>
                @if($errors->has('direccion'))
                <p class="help-block">
                    {{ $errors->first('direccion') }}
                </p>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.paciente.fields.direccion_helper') }}
                </p>
            </div>
            <div class="form-group {{ $errors->has('urban') ? 'has-error' : '' }}">
                <label for="urban">{{ trans('cruds.paciente.fields.urban') }}*</label>
                <input type="text" id="urban" name="urban" class="form-control" value="{{ old('urban', isset($paciente) ? $paciente->urban : '') }}" required>
                @if($errors->has('urban'))
                <p class="help-block">
                    {{ $errors->first('urban') }}
                </p>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.paciente.fields.urban_helper') }}
                </p>
            </div>
            <div class="form-group {{ $errors->has('cidade') ? 'has-error' : '' }}">
                <label for="cidade">{{ trans('cruds.paciente.fields.cidade') }}*</label>
                <input type="text" id="cidade" name="cidade" class="form-control" value="{{ old('cidade', isset($paciente) ? $paciente->cidade : '') }}" required>
                @if($errors->has('cidade'))
                <p class="help-block">
                    {{ $errors->first('cidade') }}
                </p>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.paciente.fields.cidade_helper') }}
                </p>
            </div>
            <div class="form-group {{ $errors->has('estado') ? 'has-error' : '' }}">
                <label for="estado">{{ trans('cruds.paciente.fields.estado') }}*</label>
                <input type="text" id="estado" name="estado" class="form-control" value="{{ old('estado', isset($paciente) ? $paciente->estado : '') }}" required>
                @if($errors->has('estado'))
                <p class="help-block">
                    {{ $errors->first('estado') }}
                </p>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.paciente.fields.estado_helper') }}
                </p>
            </div>
            <div class="form-group {{ $errors->has('observacoes') ? 'has-error' : '' }}">
                <label for="observacoes">{{ trans('cruds.paciente.fields.observacoes') }}</label>
                <textarea id="observacoes" name="observacoes" class="form-control ckeditor">{{ old('observacoes', isset($paciente) ? $paciente->observacoes : '') }}</textarea>
                @if($errors->has('observacoes'))
                    <p class="help-block">
                        {{ $errors->first('observacoes') }}
                    </p>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.paciente.fields.observacoes_helper') }}
                </p>
            </div>
            <div>
                <input class="btn btn-danger" type="submit" value="{{ trans('global.save') }}">
            </div>
        </form>
    </div>
</div>
@endsection

@section('scripts')
<script>
    var uploadedDocumentoMap = {}
Dropzone.options.documentoDropzone = {
    url: '{{ route('admin.pacientes.storeMedia') }}',
    maxFilesize: 3, // MB
    addRemoveLinks: true,
    headers: {
      'X-CSRF-TOKEN': "{{ csrf_token() }}"
    },
    params: {
      size: 3
    },
    success: function (file, response) {
      $('form').append('<input type="hidden" name="documento[]" value="' + response.name + '">')
      uploadedDocumentoMap[file.name] = response.name
    },
    removedfile: function (file) {
      file.previewElement.remove()
      var name = ''
      if (typeof file.file_name !== 'undefined') {
        name = file.file_name
      } else {
        name = uploadedDocumentoMap[file.name]
      }
      $('form').find('input[name="documento[]"][value="' + name + '"]').remove()
    },
    init: function () {
@if(isset($paciente) && $paciente->documento)
          var files =
            {!! json_encode($paciente->documento) !!}
              for (var i in files) {
              var file = files[i]
              this.options.addedfile.call(this, file)
              file.previewElement.classList.add('dz-complete')
              $('form').append('<input type="hidden" name="documento[]" value="' + file.file_name + '">')
            }
@endif
    },
     error: function (file, response) {
         if ($.type(response) === 'string') {
             var message = response //dropzone sends it's own error messages in string
         } else {
             var message = response.errors.file
         }
         file.previewElement.classList.add('dz-error')
         _ref = file.previewElement.querySelectorAll('[data-dz-errormessage]')
         _results = []
         for (_i = 0, _len = _ref.length; _i < _len; _i++) {
             node = _ref[_i]
             _results.push(node.textContent = message)
         }

         return _results
     }
}
</script>
@stop