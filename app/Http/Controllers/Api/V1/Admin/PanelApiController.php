<?php

namespace App\Http\Controllers\Api\V1\Admin;

use App\Http\Controllers\Controller;
use App\Http\Resources\Admin\PanelResource;
use App\admin;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class PanelApiController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('panel_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new PanelResource(Paciente::all());
    }

    public function show(Role $role)
    {
        abort_if(Gate::denies('panel_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new PanelResource($role->load(['permissions']));
    }

}
