<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Panel;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class PanelController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('panel_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $pacientes = Panel::all();

        return view('admin.panel.index', compact('panel'));
    }


    public function show(Panel $panel)
    {
        abort_if(Gate::denies('panel_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.panel.show', compact('panel'));
    }

}
