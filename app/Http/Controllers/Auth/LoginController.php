<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;


    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }


//Redireccion

public function redirectPath()
    {
        //$user = Auth::users()->with('rol')->get();
        $view = "";
        if (auth()->user()->email == 'terapeuta@mail.com') {
            $view = '/admin/pacientes';
        }
        if (auth()->user()->email == 'admin@mail.com') {
            $view = '/admin';
        }
        return $view;
    }

}