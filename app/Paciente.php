<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Paciente extends Model
{
    use SoftDeletes;

    public $table = 'pacientes';

    protected $dates = [
        'nascimento',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    const SEXO_RADIO = [
        'masculino' => 'Masculino',
        'feminino'  => 'Feminino',
    ];
    

    const AREA_SELECT = [
        'Obra social' => 'Obra social',
        'Otro profesional' => 'Otro profesional',
        'Personal Medico' => 'Personal médico',
        'Padres'  => 'Referencia de padres',
    ];


    

    protected $fillable = [
        'nome',
        'dni',
        'codigo',
        'nascimento',
        'sexo',
        'email',
        'area',
        'urban',
        'cidade',
        'estado',
        'direccion',
        'created_at',
        'updated_at',
        'deleted_at',
        'alergia',
        'telefono',
        'sanguineo',
        'civil',
        'observacoes',
    ];

    const G_Sanguineo = [
        'A+' => 'A+',
        'A-' => 'A-',
        'B+' => 'B+',
        'B-' => 'B-',
        'O+' => 'O+',
        'O-'  => 'O-',
        'AB+' => 'AB+',
        'AB-' => 'AB-',
    ];

    const E_Civil = [
        'Soltero' => 'Soltero',

    ];

    public function atendimentos()
    {
        return $this->hasMany(Atendimento::class, 'paciente_id', 'id');
    }

    public function getNascimentoAttribute($value)
    {
        return $value ? Carbon::parse($value)->format(config('panel.date_format')) : null;
    }

    public function setNascimentoAttribute($value)
    {
        $this->attributes['nascimento'] = $value ? Carbon::createFromFormat(config('panel.date_format'), $value)->format('Y-m-d') : null;
    }
}
