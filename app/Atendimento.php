<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Atendimento extends Model
{
    public $table = 'atendimentos';

    protected $dates = [
        'data',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $fillable = [
        'data',
        'hora',
        'peso',
        'talla',
        'presion',
        'fcard',
        'fresp',
        'temperatura',
        'created_at',
        'updated_at',
        'deleted_at',
        'paciente_id',
        'service_id',
        'observacoes'
    ];

    const A_Asistencia = [
        'noAtencion' => 'No hay atención en el día',
        'atendido'  => 'Atendido',
        'pacienteAus'  => 'Paciente ausente',
        'terapeutaAus'  => 'Terapeuta ausente',
    ];
    

    public function paciente()
    {
        return $this->belongsTo(Paciente::class, 'paciente_id');
    }

    public function service()
    {
        return $this->belongsTo(Service::class, 'service_id');
    }

    public function getDataAttribute($value)
    {
        return $value ? Carbon::parse($value)->format(config('panel.date_format')) : null;
    }

    public function setDataAttribute($value)
    {
        $this->attributes['data'] = $value ? Carbon::createFromFormat(config('panel.date_format'), $value)->format('Y-m-d') : null;
    }
}
